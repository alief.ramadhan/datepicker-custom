import { useState } from 'react'
import './App.css'
import Overlay from './components/Overlay'

function App() {
  const [showModal,setShowModal] = useState(false)
  const [selectedDate, setSelectedDate] = useState<Date | null>(null);
  

  const handleDateSelect = (date: Date) => {
    console.log(date)
    setSelectedDate(date);
    // setShowModal(false);
  };

  const formattedDate = selectedDate ? selectedDate.toISOString().split('T')[0] : '';
  console.log(formattedDate,selectedDate)

  return (
    <div className='container'>
      <div className='mobile-container'>
        <input className='custom-date-picker' type='date' readOnly onClick={() => setShowModal(true)} value={formattedDate}/>
        <Overlay isVisible={showModal} onClose={() => setShowModal(false)} onDateSelect={handleDateSelect}/>
      </div>
    </div>
  )
}

export default App
