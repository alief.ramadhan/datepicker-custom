// src/Overlay.tsx
import React, {useState, useRef, useEffect} from 'react';
import "../App.css"

interface OverlayProps {
  isVisible: boolean;
  onClose?: () => void;
  onDateSelect: (date: Date) => void; 
}

const Overlay: React.FC<OverlayProps> = ({ isVisible, onClose, onDateSelect }) => {
  const [isButtonHovered, setIsButtonHovered] = useState<boolean>(false);
  const [selectedDay, setSelectedDay] = useState<number>(28);
  const [selectedMonth, setSelectedMonth] = useState<string>('Nov');
  const [selectedYear, setSelectedYear] = useState<number>(2024);

  const days = Array.from({ length: 31 }, (_, i) => i + 1);
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const years = Array.from({ length: 100 }, (_, i) => 2000 + i)

  const dayRef = useRef<HTMLDivElement>(null);
  const monthRef = useRef<HTMLDivElement>(null);
  const yearRef = useRef<HTMLDivElement>(null);

  const scrollToCenter = (ref: React.RefObject<HTMLDivElement>, index: number) => {
    if (ref.current) {
      const container = ref.current;
      const item = container.children[index] as HTMLElement | undefined;
      if (item) {
        const containerHeight = container.clientHeight;
        const itemHeight = item.clientHeight;
        const scrollY = item.offsetTop - containerHeight / 2 + itemHeight / 2 - 18;
        container.scrollTo({ top: scrollY, behavior: 'smooth' });
      }
    }
  };

  console.log(selectedDay,selectedMonth,selectedYear)

  useEffect(() => {
    const dayIndex = days.indexOf(selectedDay);
    const monthIndex = months.indexOf(selectedMonth);
    const yearIndex = years.indexOf(selectedYear);

    scrollToCenter(dayRef, dayIndex);
    scrollToCenter(monthRef, monthIndex);
    scrollToCenter(yearRef, yearIndex);
  }, [selectedDay, selectedMonth, selectedYear]);

  const handleDayClick = (day: number) => {
    setSelectedDay(day);
  };

  const handleMonthClick = (month: string) => {
    setSelectedMonth(month);
  };

  const handleYearClick = (year: number) => {
    setSelectedYear(year);
  };

  const updateDate = () => {
    const selectedDate = parseCustomDateString(`${selectedDay} '${selectedMonth}' ${selectedYear}`);
    onDateSelect(selectedDate);
    if(onClose) {
      onClose()
    }
  };

  const parseCustomDateString = (customDateString: string): Date => {
    const [day, monthWithQuotes, year] = customDateString.split(' ');
    const month = monthWithQuotes.replace(/'/g, '');

    const months: { [key: string]: number } = {
      'Jan': 0,
      'Feb': 1,
      'Mar': 2,
      'Apr': 3,
      'May': 4,
      'Jun': 5,
      'Jul': 6,
      'Aug': 7,
      'Sep': 8,
      'Oct': 9,
      'Nov': 10,
      'Dec': 11,
    };

    const monthNumber = months[month];
    const dayNumber = parseInt(day, 10);

    // Create a date object with UTC to avoid timezone issues
    const date = new Date(Date.UTC(parseInt(year, 10), monthNumber, dayNumber));
    return date;
  };

  const handleMouseEnter = () => setIsButtonHovered(true);
  const handleMouseLeave = () => setIsButtonHovered(false);


  return (
    <div
      style={{
        ...styles.overlay,
        transform: isVisible ? 'translateY(0)' : 'translateY(100%)',
      }}
      onClick={onClose}
    >
      <div style={styles.content} onClick={(e) => e.stopPropagation()}>
        <h2>Overlay Content</h2>
        <div style={styles.datePickerContainer}>
          <ul style={styles.datePicker}>
            <li style={styles.datePickerItem}>
              <div ref={dayRef} style={{ ...styles.datePickerItemInner, ...styles.hiddenScrollBar }}>
                {days.map(day => (
                  <div
                    key={day}
                    onClick={() => handleDayClick(day)}
                    style={day === selectedDay ? { ...styles.selectedItem, ...styles.centerAlign } : styles.centerAlign}
                  >
                    {day}
                  </div>
                ))}
              </div>
            </li>
            <li style={styles.datePickerItem}>
              <div ref={monthRef} style={{ ...styles.datePickerItemInner, ...styles.hiddenScrollBar }}>
                {months.map(month => (
                  <div
                    key={month}
                    onClick={() => handleMonthClick(month)}
                    style={month === selectedMonth ? { ...styles.selectedItem, ...styles.centerAlign } : styles.centerAlign}
                  >
                    {month}
                  </div>
                ))}
              </div>
            </li>
            <li style={{ ...styles.datePickerItem, marginRight: '1.5rem' }}>
              <div ref={yearRef} style={{ ...styles.datePickerItemInner, ...styles.hiddenScrollBar }}>
                {years.map(year => (
                  <div
                    key={year}
                    onClick={() => handleYearClick(year)}
                    style={year === selectedYear ? { ...styles.selectedItem, ...styles.centerAlign } : styles.centerAlign}
                  >
                    {year}
                  </div>
                ))}
              </div>
            </li>
          </ul>
        </div>
        <button 
            onClick={updateDate} 
            style={{
                ...styles.button,
                backgroundColor: isButtonHovered ? '#FFC800' : '#FFD333'
            }}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
        >
            Pilih
        </button>
      </div>
    </div>
  );
};

const styles = {
  overlay: {
    position: 'fixed' as 'fixed',
    bottom: 0,
    left: '37.5%',
    right: 0,
    height: '40%',
    maxWidth: '360px',
    color: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
    transition: 'transform 0.4s ease-in-out',
    zIndex: 1000,
  },
  content: {
    position: 'relative' as 'relative',
    bottom: 0,
    width: '100%',
    backgroundColor: 'white',
    color: 'black',
    textAlign: 'center' as 'center',
    borderRadius: '0.5rem',
    padding: '1rem',
  },
  button: {
    width: '100%',
    marginTop: '20px',
    padding: '10px 20px',
    cursor: 'pointer',
    borderWidth: 0,
    transitionDuration: '500ms',
    backgroundColor: '#FFC800',
  },
  datePickerContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    paddingTop: '2.5rem',
    paddingBottom: '2.5rem',
    backgroundColor: '#f0f0f0',
  },
  datePicker: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    gap: '1rem'
  },
  datePickerItem: {
    listStyle: 'none',
    paddingTop: '1rem',
    paddingBottom: '1rem',
    width: '100%',
    maxHeight: '150px',
    overflowY: 'hidden' as 'hidden', 
    position: 'relative' as 'relative',
  },
  datePickerItemInner: {
    width: '100%',
    borderTop: '1px solid #000',
    borderBottom: '1px solid #000',
    display: 'flex',
    flexDirection: 'column' as 'column',
    alignItems: 'center',
    gap: '0.5rem',
    height: '50px',
    overflowY: 'scroll' as 'scroll',
    scrollbarWidth: 'none' as 'none',
    cursor: 'pointer'
  },
  selectedItem: {
    paddingTop: '1rem',
    paddingBottom: '1rem',
    display: 'relative',
    top: '50%',
    color: 'blue',
    margin: 'auto'
  },
  hiddenScrollBar: {
    scrollbarWidth: 'none' as 'none',
    msOverflowStyle: 'none' as 'none'
  },
  centerAlign: {
    height: '50px',
    lineHeight: '50px',
    textAlign: 'center' as 'center'
  }
};

export default Overlay;
